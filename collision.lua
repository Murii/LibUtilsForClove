--[[
--
--
--

Copyright 2017 Muresan Vlad Mihail

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



--
--
--]]


--- Author: Muresan Vlad Mihail known as Murii
-- version 2.0 

-- Common functions

require "helper"

local remove = table.remove

local sqrt = math.sqrt
local cos = math.cos
local atan2 = math.atan2

-- Constants

local pi = math.pi
local pi2 = pi*2

-- Partitioning

local quad = dofile(helper.PATH.."quad.lua")

local qinsert = quad.insert
local qremove = quad.remove
local qselect = quad.select
local qselectR = quad.selectRange

-- Collisions

local shape = dofile(helper.PATH.."shapes.lua")
local getBounds = shape.bounds
-- Internal data

-- list of shapes
local statics = {}
local dynamics = {}
local kinematics = {}

-- global gravity
local gravityx = 0
local gravityy = 0
-- broad phase partitioning method
local partition = "quad"
-- buffer reused in queries
local buffer = {}
-- some stats
local nchecks = 0

-- Internal functionality

-- returns shape index and its list
local function findShape(s)
    local t = s.list
    for i = 1, #t do
        if t[i] == s then
            return i, t
        end
    end
end

-- repartition moved or modified shapes
local function repartition(s)
    if partition == "quad" then
        -- reinsert in the quadtree
        local x, y, hw, hh = shape.bounds(s)
        qinsert(s, x, y, hw, hh)
    end
end

-- changes the position of a shape
local function changePosition(a, dx, dy, dt)
    shape.position(a, dx, dy, dt)
    repartition(a)
end

local function calculateFriction(d, dt)
    local _minus_x = d.friction.x * dt
    if d.xv > 0.0 then
        d.xv = d.xv - _minus_x; if d.xv < 0.0 then d.xv = 0.0 end
    end
    if d.xv < 0.0 then
        d.xv = d.xv + _minus_x; if d.xv > 0.0 then d.xv = 0.0 end
    end
    local _minus_y = d.friction.y * dt
    if d.yv > 0.0 then
        d.yv = d.yv - _minus_y; if d.yv < 0.0 then d.yv = 0.0 end
    end
    if d.yv < 0.0 then
        d.yv = d.yv + _minus_y; if d.yv > 0.0 then d.yv = 0.0 end
    end
end

-- resolves collisions
local function solveCollision(a, b, nx, ny, pen, dt)
    -- shape a must be dynamic
    --assert(a.list == dynamics, "collision pair error")
    local vx, vy = a.xv - (b.xv or 0), a.yv - (b.yv or 0)
    local dp = vx*nx + vy*ny
    -- objects moving towards each other?
    if dp < 0 then
        -- find separation velocity
        -- project velocity onto the collision normal
        local sx, sy = nx*dp, ny*dp
        -- find collision impulse
        local r = 1 + a.bounce

        local dvx = sx*r
        local dvy = sy*r
        -- adjust the velocity of shape a
        a.xv = a.xv - dvx
        a.yv = a.yv - dvy

        if b.list == dynamics then
            local arc = atan2(vy, vx) - atan2(ny, nx)
            arc = (arc + pi)%(pi2) - pi
            --assert(arc <= pi and arc >= -pi, "collision angle error")
            local p = cos(arc)
            -- adjust the velocity of shape b
            b.xv = b.xv - dvx*p
            b.yv = b.yv - dvy*p
        end
    end
    -- find the separation vector
    local sx, sy = nx*pen, ny*pen
    --assert(sx ~= 0 or sy ~= 0, "collision separation error")
    -- store the separation for shape a
    a.sx = a.sx + sx 
    a.sy = a.sy + sy 

    -- separate the pair by moving shape a
    changePosition(a, sx, sy, 1)
end

-- check and report collisions
local function checkCollision(a, b, dt)
    -- track the number of collision checks (optional)
    nchecks = nchecks + 1

    local nx, ny, pen = shape.test(a, b, dt)
    if pen == nil then
        return
    end

    --assert(pen > 0, "collision penetration error")
    -- collision callbacks
    local ra = true
    local rb = true
    if a.onCollide then
        ra = a:onCollide(a, b, nx, ny, pen)
    end
    if b.onCollide then
        rb = b:onCollide(b, a, -nx, -ny, pen)
    end
    -- ignore collision if either callback returned false
    if ra == true and rb == true and a.allowCollision and b.allowCollision then
        solveCollision(a, b, nx, ny, pen, dt)
    end
end

local function clampVelocities(d)
    if d.xv > d.maxVelocity.x then
        d.xv = d.maxVelocity.x
    end
    if d.xv < -d.maxVelocity.x then
        d.xv = -d.maxVelocity.x
    end
    if d.yv > d.maxVelocity.y then
        d.yv = d.maxVelocity.y
    end
    if d.yv < -d.maxVelocity.y then
        d.yv = -d.maxVelocity.y
    end
end

-- Public functionality
collision = {}

function collision.draw()
    for i = 1, #dynamics do 
        local d = dynamics[i]
        shape.debug(d,d.hasProperty)
    end
    for i = 1, #kinematics do 
        local d = kinematics[i]
        shape.debug(d,d.hasProperty)
    end
    for i = 1, #statics do 
        local d = statics[i]
        shape.debug(d,d.hasProperty)
    end
end

-- updates the simulation
function collision.update(dt)
    -- track the number of collision checks (optional)
    nchecks = 0
    -- update velocity vectors
    for i = 1, #dynamics do
        local d = dynamics[i]
        -- damping
        local c = 1 + d.damping
        local xv = d.xv/c
        local yv = d.yv/c
        -- gravity
        local xg = (d.gravity.x or 0) + (gravityx or 0)
        local yg = (d.gravity.y or 0) + (gravityy or 0)
        if d.allowGravity then
            xv = xv + xg*dt
            yv = yv + yg*dt
        end
        d.xv = xv
        d.yv = yv
        clampVelocities(d)
        calculateFriction(d,dt)

        -- reset separation
        d.sx = 0
        d.sy = 0
    end

    -- move kinematic shapes
    for i = 1, #kinematics do
        local k = kinematics[i]
        changePosition(k, k.xv, k.yv, dt)
    end
    -- move dynamic shapes
    if partition == 'quad' then
        -- quadtree partitioning
        for i = 1, #dynamics do
            local d = dynamics[i]
            -- move to new position
            changePosition(d, d.xv, d.yv, dt)
            -- check and resolve collisions
            -- query for potentially colliding shapes
            local x, y, hw, hh = getBounds(d)
            qselectR(x, y, hw, hh, buffer)
            -- note: we check each collision pair twice
            for j = #buffer, 1, -1 do
                local d2 = buffer[j]
                if d2 ~= d then
                    checkCollision(d, d2, dt)
                end
                -- clear the buffer during iteration
                buffer[j] = nil
            end
        end
    end
end

-- gets the global ity
function collision.getGravity()
    return gravityx, gravityy
end

-- sets the global gravity
function collision.setGravity(x, y)
    gravityx, gravityy = x, y
end

-- rect,circle,line
local function addShapeType(list, t, ...)
    assert(shape.new[t], "invalid shape type")
    local s = shape.new[t](...)
    s.list = list
    list[#list + 1] = s
    repartition(s)
    return s
end

--NOTE: ... are X,Y,W,H ! See shapes.lua for more info

-- static shapes do not move or respond to collisions
function collision.addStatic(shape,tag, ...)
    local s = addShapeType(statics, shape, ...)
    s.friction = {x=0,y=0}
    s.offset = {x=0,y=0} -- bounding box offset
    s.tag = tag
    s.allowCollision = true
    s.allowGravity = false
    s.hasProperty = "static"
    return s
end

-- dynamic shapes are affected by gravity and collisions
function collision.addDynamic(shape,tag, ...)
    local s = addShapeType(dynamics, shape, ...)
    s.friction = {x=0,y=0}
    s.bounce = 0
    s.offset = {x=0,y=0} -- bounding box offset
    s.maxVelocity = {x=1000,y=1000}
    s.gravity = {x=0,y=0}
    s.damping = 0
    s.xv, s.yv = 0, 0
    s.sx, s.sy = 0, 0
    s.tag = tag
    s.allowCollision = true
    s.allowGravity = true -- Controls wether or not the object is affected by the global gravity
    s.hasProperty = "dynamic"
    return s
end

-- kinematic shapes move only when assigned a velocity
function collision.addKinematic(shape,tag, ...)
    local s = addShapeType(kinematics, shape, ...)
    s.xv, s.yv = 0, 0
    s.tag = tag
    s.gravity = {x=0,y=0}
    s.allowGravity = true
    s.hasProperty = "kinematic"
    s.allowCollision = true
    return s
end

-- removes shape from its list
function collision.removeShape(s)
    local i, t = findShape(s)
    if i then
        s.list = nil
        remove(t, i)
        qremove(s)
    end
end

-- gets the position of a shape (starting point for line shapes)
function collision.getPosition(a)
    return a.x, a.y
end

-- sets the position of a shape
function collision.setPosition(a, x, y, dt)
    changePosition(a, x - a.x, y - a.y, dt)
end

-- gets the velocity of a shape
function collision.getVelocity(a)
    return a.xv or 0, a.yv or 0
end

-- sets the velocity of a shape
function collision.setVelocity(a, xv, yv)
    a.xv = xv
    a.yv = yv
end

function collision.getVelocityX(a)
    return a.xv 
end

function collision.getVelocityY(a)
    return a.yv 
end

function collision.setVelocityX(a, xv)
    a.xv = xv
end

function collision.setVelocityY(a, yv)
    a.yv = yv
end

-- sets the max velocity
function collision.setMaxVelocity(max)
    maxVelocity = max
end

function collision.check_collision(x1,y1,w1,h1, x2,y2,w2,h2)
    return x1 < x2+w2 and
    x2 < x1+w1 and
    y1 < y2+h2 and
    y2 < y1+h1
end

-- gets the separation of a shape for the last frame
function collision.getDisplacement(a)
    return a.sx or 0, a.sy or 0
end

function collision.setDisplacement(a,x,y)
    a.sx = x
    a.sy = y
end

-- sets the partitioning method
function collision.setPartition(p)
    assert(p == "quad" or p == "none", "invalid partitioning method")
    partition = p
end

-- gets the partitioning method
function collision.getPartition()
    return partition
end

-- gets the number of collision checks for the last frame
function collision.getCollisionCount()
    return nchecks
end

-- Public access to some tables

collision.repartition = repartition
collision.statics = statics
collision.dynamics = dynamics
collision.kinematics = kinematics

return collision
