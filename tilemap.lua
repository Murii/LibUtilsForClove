--[[
--
--
--

Copyright 2017 Muresan Vlad Mihail

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



--
--
--]]


tilemap = {}
tilemap.__index = tilemap

function tilemap.create()
    local self = {}

    self.tileWidth = 0
    self.tileHeight = 0
    self.mapWidth = 0
    self.mapHeight = 0
    self.offset = {x=0,y=0}
    self.image = nil
    self.map = nil
    self.data = {}
    self.objects_data = {}
    self.layers = {}
    self.layers.visibles = {}
    self.layers.name = "no name yet"
    self.quads = nil
    self.offsetX = 0
    self.offsetY = 0

    self.found_data = false
    self.found_objects = false

    return setmetatable(self, tilemap);
end

--[[
- path: path to tileset
- file: path to map 
- layer: layer you want to load
- quads: all the quads which represent the tiles from your tilesheet if any(nil can be passed).
--]]
function tilemap:load(path,file,layer,quads)
    self.map = dofile (file..".lua")
    self.tileWidth = self.map.tilewidth;
    self.tileHeight = self.map.tileheight;
    self.mapWidth = self.map.width;
    self.mapHeight = self.map.height;
    self.image = love.graphics.newImage(path)
    self.quads = quads
end

-- private function
function tilemap:pixelToMap (x, y, w, h, clamp)
    if type(clamp) == 'nil' then clamp = true end

    local mapX = math.floor(x / self.tileWidth) + 0
    local mapY = math.floor(y / self.tileHeight) + 0

    if clamp then
        if mapX < 0 then mapX = 0 end
        if mapY < 0 then mapY = 0 end
        if mapX > w then mapX = w end
        if mapY > h  then mapY = h end
    end

    return mapX, mapY
end

-- wip
function tilemap:addCollision(tiles,tileWidth,tileHeight,offsetX,offsetY,tag)
    offsetX = offsetX or 0
    offsetY = offsetY or 0
    tag = tag or "map"
    for y = 0, self.mapHeight   do
        for x = 1, self.mapWidth do
            for i,v in pairs(tiles) do
                if(self.data[x + y * self.mapWidth] == v) then
                    collision.addStatic("rect",tag,
                    offsetX + x * self.tileWidth,
                    offsetY + y * self.tileHeight,
                    tileWidth,
                    tileHeight)
                end
            end
        end
    end
end

--[[
-- layerType: e.g : tilelayer or objectgroup
--]]
function tilemap:init(layerNumber, layerName)
    layerNumber = layerNumber or 1
    layerName = layerName or ""
    layerType = self.map.layers[layerNumber].type

    if #self.map.layers > 1 then 
        if self.map.layers[layerNumber].name == layerName then 
            --we are multilayered
            for i,v in pairs(self.map.layers[layerNumber]) do 
                if i == "data" and not self.found_data then 
                    self.data = v
                    self.found_data = true
                end
                if i == "objects" and not self.found_objects then
                    self.objects_data = v
                    self.found_objects = true
                end
            end
        else 
            print ("Error: layer named: " .. layerName .. " of type: " .. layerType .. " does not exist!") 
        end
    end
    if #self.map.layers == 1 then
        --single layer
        for i,v in pairs(self.map.layers) do      
            if v.data ~= nil and not self.found_data then
                self.data = v.data
                self.found_data = true
            end

            if v.objects ~= nil and not self.found_objects then
                self.objects_data = v.objects
                self.found_objects = true
            end

        end
    end

end

function tilemap:getObjects()
    if #self.objects_data > 0 then
        return self.objects_data
    end
    print("Error in tilemap.lua: No objects data found on this map")
    return nil
end

function tilemap:getTile(x, y)
    return self.data[(x / self.tileWidth) + (y / self.tileHeight) * self.mapWidth]
end

--[[
- min_camX: from where to start drawing on X axis
- min_camY: from where to start drawing on Y axis
- max_camX: where to finish drawing on X axis
- max_camY: where to finish drawing on Y axis
- offsetX, offsetY: you know
- w: the width of drawn area
- h: the height of drawn area
- indexes: what tiles you want to draw. Note: you need to pass the number of tile from your array
--]]
function tilemap:draw(min_camX, min_camY, max_camX, max_camY, offsetX, offsetY, w, h, indexes)
    max_camX = math.floor(max_camX)
    max_camY  = math.floor(max_camY)
    min_camX = math.floor(min_camX)
    min_camY  = math.floor(min_camY)
    self.offsetX = offsetX
    self.offsetY = offsetY

    local startX, startY = self:pixelToMap(min_camX, min_camY, w, h)
    local endX, endY = self:pixelToMap(max_camX, max_camY, w, h)
    local quad = nil

    if type(self.quads) == "table" then    
        quad = self.quads[i]
    else 
        quad = self.quads
    end

    for y = startY,endY do
        for x = startX,endX do
            if quad ~= nil then -- in case there's not tilesheet
                for i,v in pairs(indexes) do
                    if(self.data[x + y * self.mapWidth] == v) then
                        love.graphics.draw(
                        self.image, quad, 
                        offsetX + ((x-1) * self.tileWidth), 
                        offsetY + ((y-1) * self.tileHeight))               
                    end
                end
            else 
                love.graphics.draw(self.image, 
                offsetX + ((x-1) * self.tileWidth), 
                offsetY + ((y-1) * self.tileHeight))
            end

        end
    end
end
