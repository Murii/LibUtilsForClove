--[[
--
--
--

Copyright 2017 Muresan Vlad Mihail

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



--
--
--]]


helper = {}
helper.__index = helper 

-- global variable used for collsion
helper.PATH = ""

-- path, eg: lib/ , libraries/ 
function helper.create(path_name)
    helper.PATH = path_name
    require (path_name.."anim")
    require (path_name.."camera")
    require (path_name.."camera_effects")
    require (path_name.."collision")
    require (path_name.."emitter")
    require (path_name.."fps_log")
    require (path_name.."tilemap")
    require (path_name.."tween")
end

function helper:update(dt)
    collision.update(dt)
end
