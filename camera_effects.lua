--[[
--
--
--
   
   Copyright 2017 Muresan Vlad Mihail

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.



--
--
--]]


camera_effects = {}
camera_effects.__index = camera_effects

--type = "fade"/"flash"/"shake"
function camera_effects.create(type)
	local self = {}
	self.type = type
	if self.type == "shake" then
		self.shake_timer = 0
	end
	self.finished = false -- boolean to determin whether or not the current playing effect has ended or should	
	return setmetatable(self, camera_effects)
end

function camera_effects:set_color_effect(speed,duration,color)
	if self.type == "fade" or self.type == "flash" then
		self.duration = duration or 1
		if self.type == "flash" then
			self.color = {red=0,green=0,blue=0,alpha=0}	
			self.final_color = {red=color[1],green=color[2],blue=color[3],alpha=color[4]}
		end
		self.timer = 0
		self.speed = speed or 100
		self.r, self.g, self.b, self.a = love.graphics.getColor()
	end
end

function camera_effects:set_shake(cam,for_how_long,power_x,power_y)
	if self.type == "shake" then 
		self.shake_timer = self.shake_timer + love.timer.getDelta()
		if self.shake_timer >= for_how_long then
			power_x = 0
			power_y = 0
			self.finished = true
			self.shake_timer = 0
		else
			if not self.finished then 
				cam:set_shake_offset(power_x, power_y)
			end
		end 
	end
end

function camera_effects:is_finished()
	return self.finished
end

function camera_effects:set_finished(value)
	self.finished = value
end

function camera_effects:update(dt)
	if self.type == "fade" or self.type == "flash" then
		self.timer = self.timer + dt
		if self.finished or self.timer >= self.duration then
			self.finished = true
			self.timer = 0
		end
		if self.color.red <= self.final_color.red then
			self.color.red = self.color.red + self.speed * self.timer * dt
		end
		if self.color.blue <= self.final_color.blue then
			self.color.blue = self.color.blue + self.speed * self.timer * dt
		end
		if self.color.green <= self.final_color.green then
			self.color.green = self.color.green + self.speed * self.timer * dt
		end
		if self.color.alpha <= self.final_color.alpha then
			self.color.alpha = self.color.alpha + self.speed * self.timer * dt
		end
	end
end

function camera_effects:draw()
	if self.type == "fade" or self.type == "flash" then
		if not self.finished then 
			love.graphics.setColor(self.color.red,self.color.green,self.color.blue,self.color.alpha)
			love.graphics.rectangle("fill",0,0,love.window.getWidth(),love.window.getHeight())
		else
			love.graphics.setColor(self.r, self.g, self.b, self.a)
		end
	end
end


