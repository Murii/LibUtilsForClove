(do

(= *pi2* (* (pi) 2))

(= collision-gravityx 0)
(= collision-gravityy 0)

(= collision-change-position
 (function (a dx dy dt)
    (shape-set-x a (+ (shape-x a) (* dx dt)))
    (shape-set-y a (+ (shape-y a) (* dy dt)))
  ))


(= collision-solve
 (function (a b nx ny pen dt)
    (let (vx (- (shape-velx a) (if (shape-velx b) (shape-velx b) 0))
          vy (- (shape-vely a) (if (shape-vely b) (shape-vely b) 0))
          dp (+ (* vx nx) (* vy ny)))

          (when (< dp 0)
           (let
             ;find separation velocity, project vel onto the coll normal
                (sx (* nx dp)
                 sy (* ny dp)
                 r (+ 1 (shape-bounce a))
                 dvx (* sx r)
                 dvy (* sy r))

            ; adjust velocity of shape a
            (shape-set-velx a (- (shape-velx a) dvx))
            (shape-set-vely a (- (shape-vely a) dvy))

            ; handle b dynamic
            (when (eq (shape-type b) 1)
             (let (arc (- (atan2 vx vy) (atan2 ny nx))
                   p 0)
                (= arc (- (mod (+ (pi) arc) *pi2*) (pi)))
                (= p (cos arc))
                (shape-set-velx b (- (shape-velx b) (* dvx p)))
                (shape-set-vely b (- (shape-vely b) (* dvy p)))
              ))
            );let
          );when

          (let (sx (* nx pen)
                sy (* ny pen))
            (shape-sx a (+ (shape-sx a) sx))
            (shape-sy a (+ (shape-sy a) sy))

            (collision-change-position a sx sy 1)
           );let
        );let
 )) ;collision-solve

 (= collision-check
  (function (a b dt)
   (let (ret (shape-test a b dt) nx 0 ny 0 pen 0)
    (when ret
        (= nx (vector-get ret 0) ny (vector-get ret 1) pen (vector-get ret 2))
            (collision-solve a b nx ny pen dt))
   );let
  ))

 (= collision-clamp-velocity
  (function (a)
    (shape-set-velx a (clamp
                         (shape-velx a)
                         (- (shape-max-velx a))
                         (shape-max-velx a)))
    (shape-set-vely a (clamp
                         (shape-vely a)
                         (- (shape-max-vely a))
                         (shape-max-vely a)))))

 (= collision-calculate-friction
  (function (a dt)
        (let (dtfx (* (shape-friction-x a) dt)
              dtfy (* (shape-friction-y a) dt))

         (when (> (shape-velx a) 0)
            (shape-set-velx a (- (shape-velx a) dtfx))
            (when (< (shape-velx a) 0)
             (shape-set-velx a 0)))

         (when (< (shape-velx a) 0)
           (shape-set-velx a (+ (shape-velx a) dtfx))
           (when (> (shape-velx a) 0)
             (shape-set-velx a 0)))

         (when (> (shape-vely a) 0)
           (shape-set-vely a (- (shape-vely a) dtfy))
           (when (< (shape-vely a) 0)
             (shape-set-vely a 0)))

         (when (< (shape-vely a) 0)
           (shape-set-vely a (+ (shape-vely a) dtfy))
           (when (> (shape-vely a) 0)
             (shape-set-vely a 0)))
          );let
   ));friction


 (= collision-update
  (function (dt)
    (let (i 0 d 0 j 's xv 0 xy 0 d 0 xg 0 yg 0)
       (loop (< i (vector-length shape-dynamics))
        (= d (vector-get shape-dynamics i))
        ; damping
        (= c (+ 1 (shape-damping d)))
        (= xv (/ (shape-velx d) c) yv (/ (shape-vely d) c))
        ; gravity
        (= xg (+ (shape-gravityx d) collision-gravityx) yg (+ (shape-gravityy d) collision-gravityy))
        (unless (zerop (shape-has-gravity d))
            (= xv (+ xv (* xg dt)) yv (+ yv (* yg dt))))

        (shape-set-velx d xv)
        (shape-set-vely d yv)

        (collision-clamp-velocity d)
        (collision-calculate-friction d dt)

        (shape-set-sx d 0)
        (shape-set-sy d 0)

        (dovector (j shape-static)
         (collision-check d s)
        )
        (= i (+ i 1))
        );loop
     );let
  ))

);do
