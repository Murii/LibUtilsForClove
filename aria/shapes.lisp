(do
  (= shape-x (function (o) (vector-get o 0)))
  (= shape-y (function (o) (vector-get o 1)))
  (= shape-hw (function (o) (vector-get o 2)))
  (= shape-hh (function (o) (vector-get o 3)))
  (= shape-ofx (function (o) (vector-get o 4)))
  (= shape-ofy (function (o) (vector-get o 5)))
  (= shape-left (function (o) (vector-get o 6)))
  (= shape-right (function (o) (vector-get o 7)))
  (= shape-top (function (o) (vector-get o 8)))
  (= shape-bottom (function (o) (vector-get o 9)))
  (= shape-velx (function (o) (vector-get o 10)))
  (= shape-vely (function (o) (vector-get o 11)))
  (= shape-sx (function (o) (vector-get o 12)))
  (= shape-sy (function (o) (vector-get o 13)))
  (= shape-bounce (function (o) (vector-get o 14)))
  (= shape-friction-x (function (o) (vector-get o 15)))
  (= shape-friction-y (function (o) (vector-get o 16)))
  (= shape-type (function (o) (vector-get o 17)))
  (= shape-max-velx (function (o) (vector-get o 18)))
  (= shape-max-vely (function (o) (vector-get o 19)))
  (= shape-damping (function (o) (vector-get o 20)))
  (= shape-gravityx (function (o) (vector-get o 21)))
  (= shape-gravityy (function (o) (vector-get o 22)))
  (= shape-has-gravity (function (o) (vector-get o 23)))


  (= shape-set-x (function (o v) (vector-set o 0 v)))
  (= shape-set-y (function (o v) (vector-set o 1 v)))
  (= shape-set-hw (function (o v) (vector-set o 2 v)))
  (= shape-set-hh (function (o v) (vector-set o 3 v)))
  (= shape-set-ofx (function (o v) (vector-set o 4 v)))
  (= shape-set-ofy (function (o v) (vector-set o 5 v)))
  (= shape-set-left (function (o v) (vector-set o 6 v)))
  (= shape-set-right (function (o v) (vector-set o 7 v)))
  (= shape-set-top (function (o v) (vector-set o 8 v)))
  (= shape-set-bottom (function (o v) (vector-set o v)))
  (= shape-set-velx (function (o v) (vector-set o 10 v)))
  (= shape-set-vely (function (o v) (vector-set o 11 v)))
  (= shape-set-sx (function (o v) (vector-set o 12 v)))
  (= shape-set-sy (function (o v) (vector-set o 13 v)))
  (= shape-set-bounce (function (o v) (vector-set o 14 v)))
  (= shape-set-friction-x (function (o v) (vector-set o 15 v)))
  (= shape-set-friction-y (function (o v) (vector-set o 16 v)))
  (= shape-set-type (function (o v) (vector-set o 17 v)))
  (= shape-set-max-velx (function (o v) (vector-set o 18 v)))
  (= shape-set-max-vely (function (o v) (vector-set o 19 v)))
  (= shape-set-damping (function (o v) (vector-set o 20 v)))
  (= shape-set-gravityx (function (o v) (vector-set o 21 v)))
  (= shape-set-gravityy (function (o v) (vector-set o 22 v)))
  (= shape-set-has-gravity (function (o v) (vector-set o 23 v)))

  ; holds all dynamic shapes created
  (= shape-dynamics (vector))
  (= shape-static (vector))

; rects have a center position and half-width/height extents
(= shape-new-rectangle
    (function (shape-type x y hw hh friction-x friction-y max-velocity-x max-velocity-y)
              (if (eq shape-type "dynamic")
               (= shape-type 1) (= shape-type 0))
              (unless friction-x (= friction-x 0))
              (unless friction-y (= friction-y 0))
              (unless max-velocity-x (= max-velocity-x 1000))
              (unless max-velocity-y (= max-velocity-y 1000))

              (= ret (vector x y hw hh 0 0 1 1 1 1 0 0 0 0 0
               friction-x friction-y shape-type
               max-velocity-x max-velocity-y 0 0 0 0))

              (if (eq shape-type 1)
               (vector-push shape-dynamics ret)
               (vector-push shape-static ret))
              ret))

(= shape-tests-rect-rect
    (function (a b dt)

    ; vector between the center of the rects
    (let (dx (- (+ (shape-x a) (shape-ofx a)) (+ (shape-x b) (shape-ofx b)))
          dy (- (+ (shape-y a) (shape-ofy a)) (+ (shape-y b) (shape-ofy b)))
          ; absolute distance between the center of the rects
          adx (abs dx)
          ady (abs dy)
          ; sum of the half-width extends
          shw (+ (shape-hw a) (shape-hw b))
          shh (+ (shape-hh a) (shape-hh b))
          close (or (>= adx shw) (>= ady shh))
          sx nil
          sy nil
          pen nil
         )

      ; intersection when the sum of half-width extents
      ; is less than the distance between rects
        (unless close

            ; shortest separation for both x and y axis
            (= sx (- shw adx) sy (- shh ady))

            (when (< dx 0)
              (= sx (- sx)))
            (when (< dy 0)
              (= sy (- sy)))

            ; ignore separation for explicitly defined edges (when they are set to 0)
            (when  (and (> sx 0)
                        (or (zerop (shape-left a)) (zerop (shape-right b)) ))
              (= sx 0))

            (when (and (< sx 0)
                   (or (zerop (shape-right a)) (zerop (shape-left b))))
              (= sx 0))

            (when (and (> sy 0)
                   (or (zerop (shape-bottom a)) (zerop (shape-top b))))
              (= sy 0))

            (when (and (< sy 0)
                   (or (zerop (shape-top a)) (zerop (shape-bottom b))))
              (= sy 0))


            ; ignore the longer separation axis
            ; when both sx and sy are non-zero
            (if (< (abs sx) (abs sy))
              (do
                (unless (zerop sx 0)
                  (= sy 0)))
              (do
                (unless (zerop sy 0)
                  (= sx 0))))

            ; penetration depth equals the length of the separation vector
            (= pen (sqrt (+ (* sx sx) (* sy sy))))
            (when (> pen 0)
              (vector (/ sx pen) (/ sy pen) pen))
            );unless
        );let
    ));tests-rect-rect

    (= shape-test
     (function (a b dt)
     (let
      (test (shape-tests-rect-rect a b) r nil x nil y nil p nil c nil)
    (when (and (eq test nil) (not (shape-type b) 1))
            (= test (shape-tests-rect-rect b a))
            (= c b)
            (= a b)
            (= b c)
            (= r t))

        (when test
          (= x (vector-get test 0) y (vector-get test 1) p (vector-get test 2))

    (when (and r x y)
            (= x (- x) y (- y)))

          (vector x y p))
     );let
      ));shape-test
)
