(do

(require 'shapes.lisp)
(require 'collision.lisp)

(= *DEFAULT_FONT* (love:graphics-newFont 12))

(= o1 (shape-new-rectangle "dynamic" 100 100 16 16))
(= o2 (shape-new-rectangle "static" 100 200 16 16))

(shape-set-has-gravity o1 1)
(shape-set-gravityy o1 60)

(loop (not (love:event-quit))
    (love:graphics-update)
    (love:timer-update)
    (love:keyboard-update)

    (love:graphics-print *DEFAULT_FONT* (string "FPS:" (love:timer-getFPS)) 20 20)

    (love:graphics-rectangle "fill"
         (shape-x o1) (shape-y o1)
         32 32)

    (love:graphics-rectangle "fill"
         (shape-x o2) (shape-y o2)
         32 32)

    (when (eq (love:keyboard-keypressed) (ord "s"))
      (shape-set-vely o1 50))

    (when (eq (love:keyboard-keypressed) (ord "w"))
      (shape-set-vely o1 (- 50)))

    (when (eq (love:keyboard-keypressed) (ord "d"))
      (shape-set-velx o1 50))

    (when (eq (love:keyboard-keypressed) (ord "a"))
      (shape-set-velx o1 (- 50)))


    (collision-change-position o1 (shape-velx o1) (shape-vely o1) (love:timer-getDelta))


    (collision-update (love:timer-getDelta))

    (when (collision-check o1 o2 (love:timer-getDelta))
      (print "COLL")
      )

    (love:graphics-swap)
))


