--author vlad mihail aka Murii 2014

collision = {}

-- You only need to call this function
function collision.collide(v1,vel1x,vel1y, v2)
	
local coll = collision.aabb(v1, v2)
local horizontal, vertical
local mtd
  
	if coll then
       mtd = collision.calculatemtd(v1, v2)
    end

    if mtd then
       if mtd.x ~= 0 then
          horizontal = true
       end
       if mtd.y ~= 0 then
          vertical = true
       end
    end
    if horizontal then
       vel1x = 0
       v1.x = v1.x + mtd.x
    end
    if vertical then
       v1.y = v1.y + mtd.y
       vel1y = 0
    end
end

function collision.aabb(v1,v2)

  return v1.x + v1.width > v2.x and v1.x < v2.x + v2.width and 
         v1.y + v1.height > v2.y and v1.y < v2.y + v2.height

end

--this function is like aabb but here you specificate the width and height you
--want to check
function collision.check(v1,w1,h1,v2,w2,h2)
  return v1.x + w1 > v2.x and v1.x < v2.x + w2 and 
         v1.y + h1 > v2.y and v1.y < v2.y + h2  
end

function collision.calculatemtd(v1, v2)
   local v1minx = v1.x 
   local v1miny = v1.y
   local v2minx = v2.x
   local v2miny = v2.y
   local v1maxx = v1.x + v1.width
   local v1maxy = v1.y + v1.height
   local v2maxx = v2.x + v2.width
   local v2maxy = v2.y + v2.height

   local mtd = {} 

   local left = v2minx - v1maxx
   local right = v2maxx - v1minx
   local up = v2miny - v1maxy
   local down = v2maxy - v1miny
   if left > 0 or right < 0 or up > 0 or down < 0 then --Not colliding. This is the easy part.
      return false
   end

   if math.abs(left) < right then --Determine the collision on the x and y axis
      mtd.x = left
   else
      mtd.x = right
   end

   if math.abs(up) < down then
      mtd.y = up
   else
      mtd.y = down
   end
   if math.abs(mtd.x) < math.abs(mtd.y) then
      mtd.y = 0
   else
      mtd.x = 0
   end

   return mtd
end

return collision;

