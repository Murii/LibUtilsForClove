# Made by Muresan Vlad Mihail aka Murii
# date: 11.12.2016 4:22 PM

src_dir="src"
res_dir="resources"

echo "Generating template project"
mkdir $src_dir
cd $src_dir
# make some default scripts
touch main.lua
    echo "function love.load() end function love.update(dt) end function love.draw() end" > main.lua 
touch player.lua
touch global.lua
    echo "G = {}" > global.lua
cd ..
mkdir $res_dir 
# create the resources folder and it's sub folders
cd $res_dir
mkdir sounds 
mkdir sfx
mkdir art
cd art/
mkdir player 
mkdir enemies
mkdir friendly 
mkdir guns
cd ..
mkdir maps
mkdir fonts
mkdir effects
cd ..
echo "Done generating template"
